$(document).ready(function () {
    const dataColumn = [
        "id",
        "maVoucher",
        "phanTramGiamGia",
        "ghiChu",
        "ngayTao",
        "ngayCapNhat",
        "Action"
    ];
    var table = $("#user-table").DataTable({
        // Khai báo các cột của datatable (Chú ý tên cột phải giống thuộc tính của object trong mảng đã khai báo)
        "columns": [
            { "data": dataColumn[0] },
            { "data": dataColumn[1] },
            { "data": dataColumn[2] },
            { "data": dataColumn[3] },
            { "data": dataColumn[4] },
            { "data": dataColumn[5] },
            { "data": dataColumn[6] }     
        ],
        // Ghi đè nội dung của cột (Ở ví dụ là cột cuối cùng), khi nào có data thì mới làm (ko làm với header row)
        "columnDefs": [{
            // target - 1 để chuyển về cột cuối cùng
            "targets": -1,
            "defaultContent": "<button class='info-user btn btn-info'>Chi tiết</button>"
        }],

        /*setting khác ở đây bạn có thể theo 02 cách load cả data hoặc load theo page*/
        serverSide: true,
        ajax: function (dataTable, callback, settings) {
            var orderIndex = dataTable.order[0].column;
            var orderType = dataTable.order[0].dir;
            var dataFilterBackend = {
                start: dataTable.start,
                length: dataTable.length,
                search: dataTable.search.value,
                columnOrder: dataColumn[orderIndex],
                typeOrder: orderType
            }

            $.ajax({
                url: "http://localhost:9090/vouchers",
                type: "GET",
                dataType: "json",
                contentType: "application/json",
                data: dataFilterBackend,
                success: function (responseObject) {
                    console.log(responseObject);
                    callback({
                        data: responseObject,
                        recordsTotal: responseObject.recordsTotal,
                        recordsFiltered: responseObject.recordsFiltered
                    });
                },
                error: function (xhr, textStatus) {
                    console.log(xhr);
                    console.log(textStatus);
                }
            });
        }
    });
});
